<?php

/**
 * Copyright © 2020 All rights reserved.
 * See COPYING.txt for license details.
 */

declare(strict_types=1);

namespace OnePeople\TeamMember\Model\Member;

use Magento\Framework\App\Request\DataPersistorInterface;
use OnePeople\TeamMember\Model\ResourceModel\Member\CollectionFactory;

class DataProvider extends \Magento\Ui\DataProvider\AbstractDataProvider
{

    protected $loadedData;
    protected $collection;
    protected $storeManager;
    protected $dataPersistor;


    /**
     * Constructor
     *
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param CollectionFactory $collectionFactory
     * @param DataPersistorInterface $dataPersistor
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $collectionFactory,
        DataPersistorInterface $dataPersistor,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        array $meta = [],
        array $data = []
    ) {
        $this->collection = $collectionFactory->create();
        $this->dataPersistor = $dataPersistor;
        $this->storeManager = $storeManager;
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }

    /**
     * Get data
     *
     * @return array
     */
    public function getData()
    {
        if (isset($this->loadedData)) {
            return $this->loadedData;
        }
        $items = $this->collection->getItems();

        foreach ($items as $model) {
            $this->loadedData[$model->getId()] = $model->getData();
            if ($model->getPhoto()) {
                $m['photo'][0]['name'] = str_replace("teammember/tmp/photomember/", "", $model->getPhoto());
                $m['photo'][0]['url'] = $this->getMediaUrl() . $model->getPhoto();
                $m['photo'][0]['size'] = 100;
                $tempLoad =  $this->loadedData;
                $this->loadedData[$model->getId()] = array_merge($tempLoad[$model->getId()], $m);
            }
        }

        $data = $this->dataPersistor->get('onepeople_teammember_member');
        if (!empty($data)) {
            $model = $this->collection->getNewEmptyItem();
            $model->setData($data);
            $this->loadedData[$model->getId()] = $model->getData();
            $this->dataPersistor->clear('onepeople_teammember_member');
        }


        return $this->loadedData;
    }

    public function getMediaUrl()
    {
        $mediaUrl = $this->storeManager->getStore()
            ->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
        return $mediaUrl;
    }
}
