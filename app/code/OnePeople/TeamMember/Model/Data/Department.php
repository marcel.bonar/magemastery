<?php
/**
 * Copyright © 2020 All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace OnePeople\TeamMember\Model\Data;

use OnePeople\TeamMember\Api\Data\DepartmentInterface;

class Department extends \Magento\Framework\Api\AbstractExtensibleObject implements DepartmentInterface
{

    /**
     * Get department_id
     * @return string|null
     */
    public function getDepartmentId()
    {
        return $this->_get(self::DEPARTMENT_ID);
    }

    /**
     * Set department_id
     * @param string $departmentId
     * @return \OnePeople\TeamMember\Api\Data\DepartmentInterface
     */
    public function setDepartmentId($departmentId)
    {
        return $this->setData(self::DEPARTMENT_ID, $departmentId);
    }

    /**
     * Get name
     * @return string|null
     */
    public function getName()
    {
        return $this->_get(self::NAME);
    }

    /**
     * Set name
     * @param string $name
     * @return \OnePeople\TeamMember\Api\Data\DepartmentInterface
     */
    public function setName($name)
    {
        return $this->setData(self::NAME, $name);
    }

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \OnePeople\TeamMember\Api\Data\DepartmentExtensionInterface|null
     */
    public function getExtensionAttributes()
    {
        return $this->_getExtensionAttributes();
    }

    /**
     * Set an extension attributes object.
     * @param \OnePeople\TeamMember\Api\Data\DepartmentExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \OnePeople\TeamMember\Api\Data\DepartmentExtensionInterface $extensionAttributes
    ) {
        return $this->_setExtensionAttributes($extensionAttributes);
    }

    /**
     * Get created_at
     * @return string|null
     */
    public function getCreatedAt()
    {
        return $this->_get(self::CREATED_AT);
    }

    /**
     * Set created_at
     * @param string $createdAt
     * @return \OnePeople\TeamMember\Api\Data\DepartmentInterface
     */
    public function setCreatedAt($createdAt)
    {
        return $this->setData(self::CREATED_AT, $createdAt);
    }

    /**
     * Get created_by
     * @return string|null
     */
    public function getCreatedBy()
    {
        return $this->_get(self::CREATED_BY);
    }

    /**
     * Set created_by
     * @param string $createdBy
     * @return \OnePeople\TeamMember\Api\Data\DepartmentInterface
     */
    public function setCreatedBy($createdBy)
    {
        return $this->setData(self::CREATED_BY, $createdBy);
    }

    /**
     * Get updated_at
     * @return string|null
     */
    public function getUpdatedAt()
    {
        return $this->_get(self::UPDATED_AT);
    }

    /**
     * Set updated_at
     * @param string $updatedAt
     * @return \OnePeople\TeamMember\Api\Data\DepartmentInterface
     */
    public function setUpdatedAt($updatedAt)
    {
        return $this->setData(self::UPDATED_AT, $updatedAt);
    }

    /**
     * Get updated_by
     * @return string|null
     */
    public function getUpdatedBy()
    {
        return $this->_get(self::UPDATED_BY);
    }

    /**
     * Set updated_by
     * @param string $updatedBy
     * @return \OnePeople\TeamMember\Api\Data\DepartmentInterface
     */
    public function setUpdatedBy($updatedBy)
    {
        return $this->setData(self::UPDATED_BY, $updatedBy);
    }
}

