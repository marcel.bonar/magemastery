<?php
/**
 * Copyright © 2020 All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace OnePeople\TeamMember\Model\Data;

use OnePeople\TeamMember\Api\Data\MemberInterface;

class Member extends \Magento\Framework\Api\AbstractExtensibleObject implements MemberInterface
{

    /**
     * Get member_id
     * @return string|null
     */
    public function getMemberId()
    {
        return $this->_get(self::MEMBER_ID);
    }

    /**
     * Set member_id
     * @param string $memberId
     * @return \OnePeople\TeamMember\Api\Data\MemberInterface
     */
    public function setMemberId($memberId)
    {
        return $this->setData(self::MEMBER_ID, $memberId);
    }

    /**
     * Get name
     * @return string|null
     */
    public function getName()
    {
        return $this->_get(self::NAME);
    }

    /**
     * Set name
     * @param string $name
     * @return \OnePeople\TeamMember\Api\Data\MemberInterface
     */
    public function setName($name)
    {
        return $this->setData(self::NAME, $name);
    }

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \OnePeople\TeamMember\Api\Data\MemberExtensionInterface|null
     */
    public function getExtensionAttributes()
    {
        return $this->_getExtensionAttributes();
    }

    /**
     * Set an extension attributes object.
     * @param \OnePeople\TeamMember\Api\Data\MemberExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \OnePeople\TeamMember\Api\Data\MemberExtensionInterface $extensionAttributes
    ) {
        return $this->_setExtensionAttributes($extensionAttributes);
    }

    /**
     * Get photo
     * @return string|null
     */
    public function getPhoto()
    {
        return $this->_get(self::PHOTO);
    }

    /**
     * Set photo
     * @param string $photo
     * @return \OnePeople\TeamMember\Api\Data\MemberInterface
     */
    public function setPhoto($photo)
    {
        return $this->setData(self::PHOTO, $photo);
    }

    /**
     * Get title
     * @return string|null
     */
    public function getTitle()
    {
        return $this->_get(self::TITLE);
    }

    /**
     * Set title
     * @param string $title
     * @return \OnePeople\TeamMember\Api\Data\MemberInterface
     */
    public function setTitle($title)
    {
        return $this->setData(self::TITLE, $title);
    }

    /**
     * Get quotes
     * @return string|null
     */
    public function getQuotes()
    {
        return $this->_get(self::QUOTES);
    }

    /**
     * Set quotes
     * @param string $quotes
     * @return \OnePeople\TeamMember\Api\Data\MemberInterface
     */
    public function setQuotes($quotes)
    {
        return $this->setData(self::QUOTES, $quotes);
    }
}

