<?php

/**
 * Copyright © 2020 All rights reserved.
 * See COPYING.txt for license details.
 */

declare(strict_types=1);

namespace OnePeople\TeamMember\Model;

use Magento\Framework\Api\DataObjectHelper;
use OnePeople\TeamMember\Api\Data\DepartmentInterface;
use OnePeople\TeamMember\Api\Data\DepartmentInterfaceFactory;


class Department extends \Magento\Framework\Model\AbstractModel
{

    protected $dataObjectHelper;

    protected $_eventPrefix = 'onepeople_teammember_department';
    protected $departmentDataFactory;
    protected $authSession;


    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param DepartmentInterfaceFactory $departmentDataFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param \OnePeople\TeamMember\Model\ResourceModel\Department $resource
     * @param \OnePeople\TeamMember\Model\ResourceModel\Department\Collection $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        DepartmentInterfaceFactory $departmentDataFactory,
        DataObjectHelper $dataObjectHelper,
        \OnePeople\TeamMember\Model\ResourceModel\Department $resource,
        \OnePeople\TeamMember\Model\ResourceModel\Department\Collection $resourceCollection,
        array $data = [],
        \Magento\Backend\Model\Auth\Session $authSession
    ) {
        $this->departmentDataFactory = $departmentDataFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->authSession = $authSession;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Retrieve department model with department data
     * @return DepartmentInterface
     */
    public function getDataModel()
    {
        $departmentData = $this->getData();

        $departmentDataObject = $this->departmentDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $departmentDataObject,
            $departmentData,
            DepartmentInterface::class
        );

        return $departmentDataObject;
    }

    public function beforeSave()
    {
        $createdAt = $this->getCreatedAt();
        $currentTime = date("Y-m-d H:i:s");
        if (empty($createdAt)) {
            $createdAt = $currentTime;
            $this->setCreatedAt($createdAt);
            $this->setUpdatedAt($currentTime);
        } else {
            $this->setUpdatedAt($currentTime);
        }

        $createdBy = $this->getCreatedBy();
        $currentAdminId = $this->authSession->getUser()->getId();
        if(empty($createdBy)){
            $createdBy = $currentAdminId;
            $this->setCreatedBy($currentAdminId);
            $this->setUpdatedBy($currentAdminId);
        }
        else{
            $this->setUpdatedBy($currentAdminId);
        }
    }
}
