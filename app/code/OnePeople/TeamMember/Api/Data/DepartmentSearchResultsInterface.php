<?php
/**
 * Copyright © 2020 All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace OnePeople\TeamMember\Api\Data;

interface DepartmentSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{

    /**
     * Get Department list.
     * @return \OnePeople\TeamMember\Api\Data\DepartmentInterface[]
     */
    public function getItems();

    /**
     * Set name list.
     * @param \OnePeople\TeamMember\Api\Data\DepartmentInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}

