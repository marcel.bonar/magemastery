<?php
/**
 * Copyright © 2020 All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace OnePeople\TeamMember\Api\Data;

interface MemberInterface extends \Magento\Framework\Api\ExtensibleDataInterface
{

    const NAME = 'name';
    const TITLE = 'title';
    const PHOTO = 'photo';
    const MEMBER_ID = 'member_id';
    const QUOTES = 'quotes';

    /**
     * Get member_id
     * @return string|null
     */
    public function getMemberId();

    /**
     * Set member_id
     * @param string $memberId
     * @return \OnePeople\TeamMember\Api\Data\MemberInterface
     */
    public function setMemberId($memberId);

    /**
     * Get name
     * @return string|null
     */
    public function getName();

    /**
     * Set name
     * @param string $name
     * @return \OnePeople\TeamMember\Api\Data\MemberInterface
     */
    public function setName($name);

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \OnePeople\TeamMember\Api\Data\MemberExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * Set an extension attributes object.
     * @param \OnePeople\TeamMember\Api\Data\MemberExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \OnePeople\TeamMember\Api\Data\MemberExtensionInterface $extensionAttributes
    );

    /**
     * Get photo
     * @return string|null
     */
    public function getPhoto();

    /**
     * Set photo
     * @param string $photo
     * @return \OnePeople\TeamMember\Api\Data\MemberInterface
     */
    public function setPhoto($photo);

    /**
     * Get title
     * @return string|null
     */
    public function getTitle();

    /**
     * Set title
     * @param string $title
     * @return \OnePeople\TeamMember\Api\Data\MemberInterface
     */
    public function setTitle($title);

    /**
     * Get quotes
     * @return string|null
     */
    public function getQuotes();

    /**
     * Set quotes
     * @param string $quotes
     * @return \OnePeople\TeamMember\Api\Data\MemberInterface
     */
    public function setQuotes($quotes);
}

