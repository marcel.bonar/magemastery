<?php
/**
 * Copyright © 2020 All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace OnePeople\TeamMember\Api\Data;

interface DepartmentInterface extends \Magento\Framework\Api\ExtensibleDataInterface
{

    const NAME = 'name';
    const CREATED_BY = 'created_by';
    const UPDATED_AT = 'updated_at';
    const UPDATED_BY = 'updated_by';
    const DEPARTMENT_ID = 'department_id';
    const CREATED_AT = 'created_at';

    /**
     * Get department_id
     * @return string|null
     */
    public function getDepartmentId();

    /**
     * Set department_id
     * @param string $departmentId
     * @return \OnePeople\TeamMember\Api\Data\DepartmentInterface
     */
    public function setDepartmentId($departmentId);

    /**
     * Get name
     * @return string|null
     */
    public function getName();

    /**
     * Set name
     * @param string $name
     * @return \OnePeople\TeamMember\Api\Data\DepartmentInterface
     */
    public function setName($name);

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \OnePeople\TeamMember\Api\Data\DepartmentExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * Set an extension attributes object.
     * @param \OnePeople\TeamMember\Api\Data\DepartmentExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \OnePeople\TeamMember\Api\Data\DepartmentExtensionInterface $extensionAttributes
    );

    /**
     * Get created_at
     * @return string|null
     */
    public function getCreatedAt();

    /**
     * Set created_at
     * @param string $createdAt
     * @return \OnePeople\TeamMember\Api\Data\DepartmentInterface
     */
    public function setCreatedAt($createdAt);

    /**
     * Get created_by
     * @return string|null
     */
    public function getCreatedBy();

    /**
     * Set created_by
     * @param string $createdBy
     * @return \OnePeople\TeamMember\Api\Data\DepartmentInterface
     */
    public function setCreatedBy($createdBy);

    /**
     * Get updated_at
     * @return string|null
     */
    public function getUpdatedAt();

    /**
     * Set updated_at
     * @param string $updatedAt
     * @return \OnePeople\TeamMember\Api\Data\DepartmentInterface
     */
    public function setUpdatedAt($updatedAt);

    /**
     * Get updated_by
     * @return string|null
     */
    public function getUpdatedBy();

    /**
     * Set updated_by
     * @param string $updatedBy
     * @return \OnePeople\TeamMember\Api\Data\DepartmentInterface
     */
    public function setUpdatedBy($updatedBy);
}

